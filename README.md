# ucsd_robocar_lane_detection1_pkg 

<div>

## Table of Contents
  - [**Nodes**](#nodes)
    - [lane_detection_node](#lane_detection_node)
    - [lane_guidance_node](#lane_guidance_node)
    - [ros_racer_calibration_node](#ros_racer_calibration_node)
  - [**Topics**](#topics)
  - [**Launch**](#launch)
    - [lane_detection_launch](#lane_detection_launch)
  - [**Tools**](#tools)
    - [ROS Guide Book](#ros-guide-book)
    - [Calibration Tutorial](#calibration-tutorial)
  - [**Troubleshooting**](#troubleshooting)
    - [Camera not working](#camera-not-working)
  - [**Demonstration videos**](#demonstration-videos)
    - [Lane detection example with yellow filter](#lane-detection-example-with-yellow-filter)
    - [Blue color detection](#blue-color-detection)
    - [Yellow color detection and line width specification](#yellow-color-detection-and-line-width-specification)
    - [Throttle and steering](#throttle-and-steering)
    - [Manipulating image dimensions](#manipulating-image-dimensions)

<div align="center">

## Nodes

</div>

### **lane_detection_node**

Associated file: **lane_detection.py**

Associated Topics:
- Subscribes to the **camera** [**Topics**](#topics)
- Publishes to the **centroid** [**Topics**](#topics)

This node subscribes from the **camera** [**Topics**](#topics) and uses opencv to identify line
information from the image, and publish the information of the lines centroid to the **centroid** [**Topics**](#topics). 

The color scheme is defined as follows:

- 2 contours : green bounding boxes and a blue average centroid
- 1 contour : green bounding box with a single red centroid

Below show the image post processing techniques, cv2 methods and the logic applied respectively.

<div align="center">
  <img src="filtering_process.png">
  <img src="applying_methods.png">
  <img src="applying_logic.png">
</div>

### **lane_guidance_node**

Associated file: **lane_guidance.py**

Associated Topics:
- Subscribes to the **centroid** [**Topics**](#topics)
- Publishes to the **cmd_vel** [**Topics**](#topics)

This node subscribes to the centroid topic, calculates the throttle and steering
based on the centroid value, and then publish them to their corresponding topics.
Throttle is based on error threshold specified in the [ros_racer_calibration_node](#ros_racer_calibration_node) 

| Error Threshold State| Throttle Response |
| ------ | ------ |
| error < error_threshold | car goes **faster** |
| error > error_threshold | car goes **slower** |

Steering is based on a proportional controller and the error threshold by calculating the error between the centroid (found in [**lane_detection_node**](#lane_detection_node)) and the heading of the car. 

| Error Threshold State| Steering Response |
| ------ | ------ |
| error < error_threshold | car steers **straight** |
| error > error_threshold | car steers **toward error** |

### **ros_racer_calibration_node**

Associated file: **ros_racer_calibration_node.py**

Associated Topics:
- Subscribes to the **centroid** [**Topics**](#topics)
- Publishes to the **cmd_vel** [**Topics**](#topics)

**These values are saved automatically to a configuration file, so just press** `control-c` **when the car is calibrated.**

Calibrate the camera, throttle and steering in this node by using the sliders to find:
- the right color filter 
- desired image dimmensions
- throttle values for both the optimal condtion (error = 0) and the non optimal condtion (error !=0) AKA go fast when error=0 and go slow if error !=0
- steering sensitivty change the Kp value to adjust the steering sensitivty. A value too high or low can make the car go unstable (oscillations in the cars trajectory)

| Kp value | Steering Response |
| ------ | ------ |
| as Kp --> 1 | steering more responsive |
| as Kp --> 0 | steering less responsive |


| Property   | Info |
| ----------  | --------------------- |
| lowH, highH | Setting low and high values for Hue  | 
| lowS, highS | Setting low and high values for Saturation | 
| lowV, highV | Setting low and high values for Value | 
| Inverted_filter | Specify to create an inverted color tracker | 
| min_width, max_width | Specify the width range of the line to be detected  | 
| number_of_lines | Specify the number of lines to be detected  | 
| error_threshold | Specify the acceptable error the robot will consider as approximately "no error" | 
| frame_width | Specify the width of image frame (horizontal cropping) | 
| rows_to_watch | Specify the number of rows (in pixels) to watch (vertical cropping) | 
| rows_offset | Specify the offset of the rows to watch (vertical pan) | 
| Steering_sensitivity | Specify the proportional gain of the steering | 
| Steering_value | Specify the steering value | 
| Throttle_mode | Toggle this slider at the end of calibration to the following 3 modes. |
| Throttle_mode 0 | zero_throttle_mode (find value where car does not move) 
| Throttle_mode 1 | zero_error_throttle_mode (find value for car to move when there is **no error** in steering)
| Throttle_mode 2 | error_throttle_mode(find value for car to move when there is **some error** in steering)| 
| Throttle_value | Specify the throttle value to be set in each of the throttle modes| 

More morphological transfromations and examples can be found <a href="https://docs.opencv.org/3.4/db/df6/tutorial_erosion_dilatation.html" >here</a> and <a href="https://docs.opencv.org/master/d9/d61/tutorial_py_morphological_ops.html" >here</a>

<div align="center">

## Topics

</div>

| Nodes | Subscribed Topics | Published Topics |
| ------ | ------ | ------ |
| lane_detection_node | /camera/color/image_raw | /centroid |
| lane_guidance_node  | /centroid               | /cmd_vel |
| calibration_node    | /camera/color/image_raw | /cmd_vel  |

<div align="center">

## Launch

</div>


#### **lane_detection_launch**

Associated file: **lane_detection_launch.launch**

This file will launch [**lane_detection_node**](#lane_detection_node), [**lane_guidance_node**](#lane_guidance_node), [**camera_server**](#camera_server) and load the color filter parameters created using [ros_racer_calibration_node](#ros_racer_calibration_node)

**Before launching, please calibrate the robot first while on the stand!**

`roslaunch ucsd_robocar_lane_detection1_pkg lane_detection_launch.launch`


<div align="center">

## Tools 

</div>

#### ROS Guide Book

For help with using ROS in the terminal and in console scripts, check out this google doc <a href="https://docs.google.com/document/d/1u7XS7B-Rl_emK3kVKEfc0MxHtwXGYHf5HfLlnX8Ydiw/edit?usp=sharing" >here</a> to see tables of ROS commands and plenty of examples of using ROS in console scripts.


#### Calibration Tutorial

Check out this google doc <a href="https://docs.google.com/document/d/1YS5YGbo8evIo9Mlb0J-w2r3bZfju37Zl4UmdaN2CD2A/edit#heading=h.6511nudcf16l" >here</a> for a tutorial that will walk through the calibration process step by step and explain more about the sliders and their effects.

<div align="center">

## Troubleshooting

</div>

#### **Camera not working** 

If while running [ros_racer_calibration_node](#ros_racer_calibration_node) or [lane_detection_launch](#lane_detection_launch) and if the cv2 windows do not open, then follow the procedure below to potentially resolve the issue.

1. Make sure camera is plugged in all the way into its USB socket
1. See if image feed is coming through in another application like cheese. (Enter `cheese` into terminal window)
1. Check to see if the camera topic is publishing data `rostopic echo /camera_rgb`
1. Restart ROS core 
1. Reboot if none of the above worked and try again `sudo reboot now`

If the camera is still not working after trying the procedure above, then it could be a hardware issue. (Did the car crash?)

## **Demonstration videos** 

</div>

<div align="center">

#### Lane detection example with yellow filter

[![lane detection example with yellow filter](https://j.gifs.com/6WRqXN.gif)](https://youtu.be/f4VrncQ7HU)

</div>

<div align="center">

#### Number of lines to detect
[![Number of lines to detect](https://j.gifs.com/qQ7Lvk.gif)](https://youtu.be/5AVL68BTD0U)

</div>

<div align="center">

#### Error threshold
[![Error threshold](https://j.gifs.com/k28Xmv.gif)](https://youtu.be/ied1TDvpDK4)

</div>

<div align="center">

#### Blue color detection

[![Blue color detection](https://j.gifs.com/PjZoj6.gif)](https://youtu.be/c9rkRHGGea0)

</div>

<div align="center">

#### Yellow color detection and line width specification

[![Yellow color detection and line width specification](https://j.gifs.com/BrLJro.gif)](https://youtu.be/l2Ngtd461DY)

</div>

<div align="center">

#### Throttle and steering

[![Throttle and steering](https://j.gifs.com/362n6p.gif)](https://youtu.be/lhYzs5v6jtI)

</div>

<div align="center">

#### Manipulating image dimensions

[![Manipulating image dimensions](https://j.gifs.com/lR5oR1.gif)](https://youtu.be/_DhG6dduYPs)

</div>
